import akka.actor.{Props, ActorSystem}
import akka.contrib.throttle.Throttler._
import scala.util.control.Breaks
import util.control.Breaks._
import akka.contrib.throttle.TimerBasedThrottler
import com.mongodb.casbah.commons.conversions.scala.{RegisterConversionHelpers, RegisterJodaTimeConversionHelpers}
import dao._
import helpers.Helper
import org.joda.time.DateTime
import scala.collection.mutable
import scala.concurrent.duration._


import scala.concurrent.duration.Duration

object Test extends App {


  RegisterConversionHelpers()
  RegisterJodaTimeConversionHelpers()

  def testCsvParse() = {
    val result = "\"Symbol\",\"Name\",\"LastSale\",\"MarketCap\",\"IPOyear\",\"Sector\",\"industry\",\"Summary dao.Quote\",\n\"TFSC\",\"1347 Capital Corp.\",\"9.65\",\"$57.4M\",\"2014\",\"Finance\",\"Business Services\",\"http://www.nasdaq.com/symbol/tfsc\",\n\"TFSCR\",\"1347 Capital Corp.\",\"0.3995\",\"n/a\",\"2014\",\"Finance\",\"Business Services\",\"http://www.nasdaq.com/symbol/tfscr\",\n\"TFSCU\",\"1347 Capital Corp.\",\"10.15\",\"$42.43M\",\"2014\",\"n/a\",\"n/a\",\"http://www.nasdaq.com/symbol/tfscu\",\n\"TFSCW\",\"1347 Capital Corp.\",\"0.2531\",\"n/a\",\"2014\",\"Finance\",\"Business Services\",\"http://www.nasdaq.com/symbol/tfscw\",\n\"PIH\",\"1347 Property Insurance Holdings, Inc.\",\"7.84\",\"$49.85M\",\"2014\",\"Finance\",\"Property-Casualty Insurers\",\"http://www.nasdaq.com/symbol/pih\",\n\"FLWS\",\"1-800 FLOWERS.COM, Inc.\",\"9.95\",\"$649.36M\",\"1999\",\"Consumer Services\",\"Other Specialty Stores\",\"http://www.nasdaq.com/symbol/flws\",\n\"FCTY\",\"1st Century Bancshares, Inc\",\"7.1601\",\"$72.79M\",\"n/a\",\"Finance\",\"Major Banks\",\"http://www.nasdaq.com/symbol/fcty\",\n\"FCCY\",\"1st Constitution Bancorp (NJ)\",\"11.4\",\"$85.55M\",\"n/a\",\"Finance\",\"Savings Institutions\",\"http://www.nasdaq.com/symbol/fccy\",\n\"SRCE\",\"1st Source Corporation\",\"33.57\",\"$800.75M\",\"n/a\",\"Finance\",\"Major Banks\",\"http://www.nasdaq.com/symbol/srce\",\n\"VNET\",\"21Vianet Group, Inc.\",\"21.71\",\"$1.43B\",\"2011\",\"Technology\",\"Computer Software: Programming, Data Processing\",\"http://www.nasdaq.com/symbol/vnet\",\n\"TWOU\",\"2U, Inc.\",\"28.09\",\"$1.16B\",\"2014\",\"Technology\",\"Computer Software: Prepackaged Software\",\"http://www.nasdaq.com/symbol/twou\",\n\"DGLD\",\"3X Inverse Gold ETN Velocityshares\",\"73.28\",\"$9.97M\",\"n/a\",\"Finance\",\"Investment Bankers/Brokers/Service\",\"http://www.nasdaq.com/symbol/dgld\",\n\"JOBS\",\"51job, Inc.\",\"35.15\",\"$2.08B\",\"2004\",\"Technology\",\"Diversified Commercial Services\",\"http://www.nasdaq.com/symbol/jobs\",\n\"SIXD\",\"6D Global Technologies, Inc.\",\"9.18\",\"$718.01M\",\"n/a\",\"Consumer Services\",\"Professional Services\",\"http://www.nasdaq.com/symbol/sixd\",\n\"EGHT\",\"8x8 Inc\",\"9.07\",\"$799.54M\",\"n/a\",\"Public Utilities\",\"Telecommunications Equipment\",\"http://www.nasdaq.com/symbol/eght\",\n\"AVHI\",\"A V Homes, Inc.\",\"16.04\",\"$358.28M\",\"n/a\",\"Capital Goods\",\"Homebuilding\",\"http://www.nasdaq.com/symbol/avhi\",\n\"SHLM\",\"A. Schulman, Inc.\",\"44.43\",\"$1.3B\",\"1972\",\"Basic Industries\",\"Major Chemicals\",\"http://www.nasdaq.com/symbol/shlm\",\n\"AAON\",\"AAON, Inc.\",\"24\",\"$1.3B\",\"n/a\",\"Capital Goods\",\"Industrial Machinery/Components\",\"http://www.nasdaq.com/symbol/aaon\",\n\"ABAX\",\"ABAXIS, Inc.\",\"51.36\",\"$1.16B\",\"1992\",\"Capital Goods\",\"Industrial Machinery/Components\",\"http://www.nasdaq.com/symbol/abax\",\n\"ABY\",\"Abengoa Yield plc\",\"35.51\",\"$2.84B\",\"2014\",\"Public Utilities\",\"Electric Utilities: Central\",\"http://www.nasdaq.com/symbol/aby\",\n\"ABGB\",\"Abengoa, S.A.\",\"16.82\",\"$2.82B\",\"2013\",\"Consumer Services\",\"Military/Government/Technical\",\"http://www.nasdaq.com/symbol/abgb\",\n\"ABMD\",\"ABIOMED, Inc.\",\"63.65\",\"$2.66B\",\"n/a\",\"Health Care\",\"Medical/Dental Instruments\",\"http://www.nasdaq.com/symbol/abmd\",\n\"AXAS\",\"Abraxas Petroleum Corporation\",\"3.01\",\"$319.64M\",\"n/a\",\"Energy\",\"Oil & Gas Production\",\"http://www.nasdaq.com/symbol/axas\",\n\"ACTG\",\"Acacia Research Corporation\",\"9.84\",\"$502.33M\",\"n/a\",\"Miscellaneous\",\"Multi-Sector Companies\",\"http://www.nasdaq.com/symbol/actg\",\n\"ACHC\",\"Acadia Healthcare Company, Inc.\",\"71.96\",\"$5.15B\",\"n/a\",\"Health Care\",\"Medical Specialities\",\"http://www.nasdaq.com/symbol/achc\",\n\"ACAD\",\"ACADIA Pharmaceuticals Inc.\",\"39.4\",\"$3.95B\",\"2004\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/acad\",\n\"ACST\",\"Acasti Pharma, Inc.\",\"0.293\",\"$31.19M\",\"n/a\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/acst\",\n\"AXDX\",\"Accelerate Diagnostics, Inc.\",\"26.72\",\"$1.19B\",\"n/a\",\"Capital Goods\",\"Biotechnology: Laboratory Analytical Instruments\",\"http://www.nasdaq.com/symbol/axdx\",\n\"XLRN\",\"Acceleron Pharma Inc.\",\"31.6\",\"$1.04B\",\"2013\",\"Health Care\",\"Biotechnology: Biological Products (No Diagnostic Substances)\",\"http://www.nasdaq.com/symbol/xlrn\",\n\"ANCX\",\"Access National Corporation\",\"21.58\",\"$227.01M\",\"n/a\",\"Finance\",\"Savings Institutions\",\"http://www.nasdaq.com/symbol/ancx\",\n\"ARAY\",\"Accuray Incorporated\",\"7.07\",\"$558.44M\",\"2007\",\"Health Care\",\"Medical/Dental Instruments\",\"http://www.nasdaq.com/symbol/aray\",\n\"VXDN\",\"AccuShares Spot CBOE VIX Down Shares\",\"22.35\",\"n/a\",\"n/a\",\"n/a\",\"n/a\",\"http://www.nasdaq.com/symbol/vxdn\",\n\"VXUP\",\"AccuShares Spot CBOE VIX Up Shares\",\"27.51\",\"n/a\",\"n/a\",\"n/a\",\"n/a\",\"http://www.nasdaq.com/symbol/vxup\",\n\"ACRX\",\"AcelRx Pharmaceuticals, Inc.\",\"4.04\",\"$179.11M\",\"2011\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/acrx\",\n\"ACET\",\"Aceto Corporation\",\"24.45\",\"$711.88M\",\"n/a\",\"Health Care\",\"Other Pharmaceuticals\",\"http://www.nasdaq.com/symbol/acet\",\n\"AKAO\",\"Achaogen, Inc.\",\"5.67\",\"$102.34M\",\"2014\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/akao\",\n\"ACHN\",\"Achillion Pharmaceuticals, Inc.\",\"8.24\",\"$968.69M\",\"2006\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/achn\",\n\"ACIW\",\"ACI Worldwide, Inc.\",\"25.35\",\"$2.96B\",\"n/a\",\"Technology\",\"Computer Software: Prepackaged Software\",\"http://www.nasdaq.com/symbol/aciw\",\n\"ACNB\",\"ACNB Corporation\",\"20.74\",\"$124.86M\",\"n/a\",\"Finance\",\"Major Banks\",\"http://www.nasdaq.com/symbol/acnb\",\n\"ACOR\",\"Acorda Therapeutics, Inc.\",\"31.55\",\"$1.35B\",\"2006\",\"Health Care\",\"Biotechnology: Biological Products (No Diagnostic Substances)\",\"http://www.nasdaq.com/symbol/acor\",\n\"ACFN\",\"Acorn Energy, Inc.\",\"0.55\",\"$14.56M\",\"n/a\",\"Consumer Services\",\"Military/Government/Technical\",\"http://www.nasdaq.com/symbol/acfn\",\n\"ACTS\",\"Actions Semiconductor Co., Ltd.\",\"1.67\",\"$143.62M\",\"2005\",\"Technology\",\"Semiconductors\",\"http://www.nasdaq.com/symbol/acts\",\n\"ACPW\",\"Active Power, Inc.\",\"2.04\",\"$47.14M\",\"2000\",\"Public Utilities\",\"Electric Utilities: Central\",\"http://www.nasdaq.com/symbol/acpw\",\n\"ATVI\",\"Activision Blizzard, Inc\",\"25.4\",\"$18.45B\",\"n/a\",\"Technology\",\"Computer Software: Prepackaged Software\",\"http://www.nasdaq.com/symbol/atvi\",\n\"ACTA\",\"Actua Corporation\",\"13.17\",\"$534.61M\",\"n/a\",\"Technology\",\"EDP Services\",\"http://www.nasdaq.com/symbol/acta\",\n\"ACUR\",\"Acura Pharmaceuticals, Inc.\",\"0.8723\",\"$42.93M\",\"n/a\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/acur\",\n\"ACXM\",\"Acxiom Corporation\",\"18.1\",\"$1.41B\",\"n/a\",\"Technology\",\"EDP Services\",\"http://www.nasdaq.com/symbol/acxm\",\n\"ADMS\",\"Adamas Pharmaceuticals, Inc.\",\"19.45\",\"$346.66M\",\"2014\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/adms\",\n\"ADMP\",\"Adamis Pharmaceuticals Corporation\",\"4.25\",\"$57.05M\",\"n/a\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/admp\",\n\"ADAP\",\"Adaptimmune Therapeutics plc\",\"19.29\",\"$217.01M\",\"2015\",\"Health Care\",\"Biotechnology: Biological Products (No Diagnostic Substances)\",\"http://www.nasdaq.com/symbol/adap\",\n\"ADUS\",\"Addus HomeCare Corporation\",\"28.61\",\"$317.27M\",\"2009\",\"Health Care\",\"Medical/Nursing Services\",\"http://www.nasdaq.com/symbol/adus\",\n\"AEY\",\"ADDvantage Technologies Group, Inc.\",\"2.35\",\"$23.67M\",\"n/a\",\"Consumer Services\",\"Office Equipment/Supplies/Services\",\"http://www.nasdaq.com/symbol/aey\",\n\"ADEP\",\"Adept Technology, Inc.\",\"6.09\",\"$80.22M\",\"n/a\",\"Technology\",\"Industrial Machinery/Components\",\"http://www.nasdaq.com/symbol/adep\",\n\"ADMA\",\"ADMA Biologics Inc\",\"8.57\",\"$91.75M\",\"n/a\",\"Health Care\",\"Biotechnology: Biological Products (No Diagnostic Substances)\",\"http://www.nasdaq.com/symbol/adma\",\n\"ADBE\",\"Adobe Systems Incorporated\",\"79.86\",\"$39.95B\",\"1986\",\"Technology\",\"Computer Software: Prepackaged Software\",\"http://www.nasdaq.com/symbol/adbe\",\n\"ADTN\",\"ADTRAN, Inc.\",\"16.82\",\"$896.83M\",\"1994\",\"Public Utilities\",\"Telecommunications Equipment\",\"http://www.nasdaq.com/symbol/adtn\",\n\"ADRO\",\"Aduro Biotech, Inc.\",\"33.78\",\"$2.1B\",\"2015\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/adro\",\n\"AEIS\",\"Advanced Energy Industries, Inc.\",\"29.04\",\"$1.19B\",\"1995\",\"Capital Goods\",\"Industrial Machinery/Components\",\"http://www.nasdaq.com/symbol/aeis\",\n\"AMD\",\"Advanced Micro Devices, Inc.\",\"2.31\",\"$1.8B\",\"n/a\",\"Technology\",\"Semiconductors\",\"http://www.nasdaq.com/symbol/amd\",\n\"ADXS\",\"Advaxis, Inc.\",\"23.78\",\"$716.31M\",\"n/a\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/adxs\",\n\"ADXSW\",\"Advaxis, Inc.\",\"18.98\",\"n/a\",\"n/a\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/adxsw\",\n\"ADVS\",\"Advent Software, Inc.\",\"43.63\",\"$2.3B\",\"1995\",\"Technology\",\"EDP Services\",\"http://www.nasdaq.com/symbol/advs\",\n\"MULT\",\"AdvisorShares Sunrise Global Multi-Strategy ETF\",\"23.83\",\"$2.38M\",\"n/a\",\"n/a\",\"n/a\",\"http://www.nasdaq.com/symbol/mult\",\n\"YPRO\",\"AdvisorShares YieldPro ETF\",\"23.03\",\"$65.64M\",\"n/a\",\"n/a\",\"n/a\",\"http://www.nasdaq.com/symbol/ypro\",\n\"AEGR\",\"Aegerion Pharmaceuticals, Inc.\",\"18.77\",\"$536.29M\",\"2010\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/aegr\",\n\"AEGN\",\"Aegion Corp\",\"18.32\",\"$671.75M\",\"n/a\",\"Basic Industries\",\"Water Supply\",\"http://www.nasdaq.com/symbol/aegn\",\n\"AEHR\",\"Aehr Test Systems\",\"2.17\",\"$27.58M\",\"1997\",\"Capital Goods\",\"Electrical Products\",\"http://www.nasdaq.com/symbol/aehr\",\n\"AMTX\",\"Aemetis, Inc\",\"4.04\",\"$80.22M\",\"n/a\",\"Basic Industries\",\"Major Chemicals\",\"http://www.nasdaq.com/symbol/amtx\",\n\"AEPI\",\"AEP Industries Inc.\",\"56.32\",\"$287.38M\",\"1986\",\"Capital Goods\",\"Specialty Chemicals\",\"http://www.nasdaq.com/symbol/aepi\",\n\"AERI\",\"Aerie Pharmaceuticals, Inc.\",\"13.03\",\"$331.27M\",\"2013\",\"Health Care\",\"Biotechnology: Biological Products (No Diagnostic Substances)\",\"http://www.nasdaq.com/symbol/aeri\",\n\"AVAV\",\"AeroVironment, Inc.\",\"27.09\",\"$632.03M\",\"2007\",\"Capital Goods\",\"Aerospace\",\"http://www.nasdaq.com/symbol/avav\",\n\"AEZS\",\"AEterna Zentaris Inc.\",\"0.282\",\"$33.26M\",\"n/a\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/aezs\",\n\"AFMD\",\"Affimed N.V.\",\"11.53\",\"$342.83M\",\"2014\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/afmd\",\n\"AFFX\",\"Affymetrix, Inc.\",\"11.63\",\"$899.28M\",\"1996\",\"Capital Goods\",\"Biotechnology: Laboratory Analytical Instruments\",\"http://www.nasdaq.com/symbol/affx\",\n\"AGEN\",\"Agenus Inc.\",\"9.48\",\"$797.82M\",\"2000\",\"Health Care\",\"Biotechnology: Biological Products (No Diagnostic Substances)\",\"http://www.nasdaq.com/symbol/agen\",\n\"AGRX\",\"Agile Therapeutics, Inc.\",\"9.14\",\"$202.93M\",\"2014\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/agrx\",\n\"AGYS\",\"Agilysys, Inc.\",\"9.3\",\"$211.76M\",\"n/a\",\"Technology\",\"EDP Services\",\"http://www.nasdaq.com/symbol/agys\",\n\"AGIO\",\"Agios Pharmaceuticals, Inc.\",\"110.13\",\"$4.11B\",\"2013\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/agio\",\n\"AIRM\",\"Air Methods Corporation\",\"41.63\",\"$1.63B\",\"n/a\",\"Transportation\",\"Transportation Services\",\"http://www.nasdaq.com/symbol/airm\",\n\"AIRT\",\"Air T, Inc.\",\"19.895\",\"$47.2M\",\"n/a\",\"Transportation\",\"Air Freight/Delivery Services\",\"http://www.nasdaq.com/symbol/airt\",\n\"ATSG\",\"Air Transport Services Group, Inc\",\"10.49\",\"$684.31M\",\"n/a\",\"Transportation\",\"Air Freight/Delivery Services\",\"http://www.nasdaq.com/symbol/atsg\",\n\"AMCN\",\"AirMedia Group Inc\",\"7.26\",\"$435.39M\",\"2007\",\"Technology\",\"Advertising\",\"http://www.nasdaq.com/symbol/amcn\",\n\"AIXG\",\"Aixtron SE\",\"7.64\",\"$861.04M\",\"n/a\",\"Technology\",\"Industrial Machinery/Components\",\"http://www.nasdaq.com/symbol/aixg\",\n\"AKAM\",\"Akamai Technologies, Inc.\",\"73.44\",\"$13.11B\",\"1999\",\"Miscellaneous\",\"Business Services\",\"http://www.nasdaq.com/symbol/akam\",\n\"AKBA\",\"Akebia Therapeutics, Inc.\",\"7.91\",\"$228.1M\",\"2014\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/akba\",\n\"AKER\",\"Akers Biosciences Inc\",\"4.415\",\"$22.71M\",\"2014\",\"Health Care\",\"Biotechnology: In Vitro & In Vivo Diagnostic Substances\",\"http://www.nasdaq.com/symbol/aker\",\n\"AKRX\",\"Akorn, Inc.\",\"46.29\",\"$5.3B\",\"n/a\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/akrx\",\n\"ALSK\",\"Alaska Communications Systems Group, Inc.\",\"2.4\",\"$120.57M\",\"1999\",\"Public Utilities\",\"Telecommunications Equipment\",\"http://www.nasdaq.com/symbol/alsk\",\n\"AMRI\",\"Albany Molecular Research, Inc.\",\"19.84\",\"$657.07M\",\"1999\",\"Health Care\",\"Biotechnology: Commercial Physical & Biological Resarch\",\"http://www.nasdaq.com/symbol/amri\",\n\"ABDC\",\"Alcentra Capital Corp.\",\"13.78\",\"$186.26M\",\"n/a\",\"n/a\",\"n/a\",\"http://www.nasdaq.com/symbol/abdc\",\n\"ADHD\",\"Alcobra Ltd.\",\"7.5\",\"$158.83M\",\"2013\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/adhd\",\n\"ALDR\",\"Alder BioPharmaceuticals, Inc.\",\"47.39\",\"$1.8B\",\"2014\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/aldr\",\n\"ALDX\",\"Aldeyra Therapeutics, Inc.\",\"8.17\",\"$78.35M\",\"2014\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/aldx\",\n\"ALXN\",\"Alexion Pharmaceuticals, Inc.\",\"169.04\",\"$33.74B\",\"1996\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/alxn\",\n\"ALXA\",\"Alexza Pharmaceuticals, Inc.\",\"1.22\",\"$23.7M\",\"2006\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/alxa\",\n\"ALCO\",\"Alico, Inc.\",\"48.44\",\"$400.96M\",\"n/a\",\"Consumer Non-Durables\",\"Farming/Seeds/Milling\",\"http://www.nasdaq.com/symbol/alco\",\n\"ALGN\",\"Align Technology, Inc.\",\"63.02\",\"$5.09B\",\"2001\",\"Health Care\",\"Industrial Specialties\",\"http://www.nasdaq.com/symbol/algn\",\n\"ALIM\",\"Alimera Sciences, Inc.\",\"4.53\",\"$201.12M\",\"2010\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/alim\",\n\"ALKS\",\"Alkermes plc\",\"58.67\",\"$8.72B\",\"1991\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/alks\",\n\"ALGT\",\"Allegiant Travel Company\",\"165.25\",\"$2.83B\",\"2006\",\"Transportation\",\"Air Freight/Delivery Services\",\"http://www.nasdaq.com/symbol/algt\",\n\"ALLB\",\"Alliance Bancorp, Inc. of Pennsylvania\",\"22.28\",\"$89.71M\",\"n/a\",\"Finance\",\"Savings Institutions\",\"http://www.nasdaq.com/symbol/allb\",\n\"AFOP\",\"Alliance Fiber Optic Products, Inc.\",\"20.52\",\"$363.77M\",\"2000\",\"Technology\",\"Semiconductors\",\"http://www.nasdaq.com/symbol/afop\",\n\"AIQ\",\"Alliance HealthCare Services, Inc.\",\"18.55\",\"$199.38M\",\"2001\",\"Health Care\",\"Medical Specialities\",\"http://www.nasdaq.com/symbol/aiq\",\n\"AHGP\",\"Alliance Holdings GP, L.P.\",\"44.27\",\"$2.65B\",\"2006\",\"Energy\",\"Coal Mining\",\"http://www.nasdaq.com/symbol/ahgp\",\n\"ARLP\",\"Alliance Resource Partners, L.P.\",\"26.44\",\"$1.96B\",\"1999\",\"Energy\",\"Coal Mining\",\"http://www.nasdaq.com/symbol/arlp\",\n\"AHPI\",\"Allied Healthcare Products, Inc.\",\"1.45\",\"$11.64M\",\"1992\",\"Health Care\",\"Industrial Specialties\",\"http://www.nasdaq.com/symbol/ahpi\",\n\"AMOT\",\"Allied Motion Technologies, Inc.\",\"26.15\",\"$242.9M\",\"n/a\",\"Capital Goods\",\"Electrical Products\",\"http://www.nasdaq.com/symbol/amot\",\n\"ALQA\",\"Alliqua BioMedical, Inc.\",\"5.44\",\"$132.72M\",\"n/a\",\"Health Care\",\"Medical/Dental Instruments\",\"http://www.nasdaq.com/symbol/alqa\",\n\"ALLT\",\"Allot Communications Ltd.\",\"7.85\",\"$261.89M\",\"2006\",\"Technology\",\"Computer Communications Equipment\",\"http://www.nasdaq.com/symbol/allt\",\n\"MDRX\",\"Allscripts Healthcare Solutions, Inc.\",\"14.15\",\"$2.56B\",\"n/a\",\"Technology\",\"EDP Services\",\"http://www.nasdaq.com/symbol/mdrx\","
    val list1 = CsvParser.parse(result)
    for (list <- list1) {
      println(list)
    }
  }

  val system = ActorSystem("StockDownload")

  def testStockListDownload() = {
    val result = StockListDownloader.process
    for (stock <- result) {
      StockDao.save(stock)
    }
    result
  }

  def testHistoricalDataDownload(stocks: List[Stock]) = {
    // default Actor constructor
    val historicalDataDownloader = system.actorOf(Props[HistoricalDataDownloader], name = "historicalDataDownloader")
    val throttler = system.actorOf(Props(classOf[TimerBasedThrottler],
      4 msgsPer Duration(1, SECONDS)))
    throttler ! SetTarget(Some(historicalDataDownloader))
    for (stock <- stocks) {
      try {
        val symbol = stock.symbol

        throttler ! DownloadHistoricalData(symbol)

      } catch {
        case e: Exception => e.printStackTrace()
      }
    }
  }

  def testDailyQuoteDownload(stocks: List[Stock]) = {
    val dailyQuoteDownloader = system.actorOf(Props[DailyQuoteDownloader], name = "dailyQuoteDownloader")
    val throttler = system.actorOf(Props(classOf[TimerBasedThrottler],
      4 msgsPer Duration(1, SECONDS)))
    throttler ! SetTarget(Some(dailyQuoteDownloader))

    for (stock <- stocks) {
      try {
        val symbol = stock.symbol
        throttler ! DownloadDailyQuote(symbol)
      } catch {
        case e: Exception => e.printStackTrace()
      }
    }

  }

  def findDailyQuote() = {
    val dailyQuote = DailyQuoteDao.findOneById("TWTR") match {
      case Some(obj) => obj
      case _ => null
    }
    println(dailyQuote)
  }

  def getAllDailyQuote: List[Quote] = {
    val date = new DateTime().withDate(2015, 6, 18).withTimeAtStartOfDay()

    val quotes = QuoteDao.findByDate(date)
    for (quote <- quotes) {
      println(quote.quoteKey.symbol)
    }
    quotes
  }

  val l = List(1, 2, 3, 4, 5)

  def g(v: Int) = {
    val x = List(v -> v * 2)
    x
  }

  val x = l.map(x => g(x))
  println(x)
  val y = l.flatMap(x => g(x))
  println(y)

  //val results = StockScreener.filter(0.1)
  def toInt(s: String): Option[Int] = {
    try {
      Some(Integer.parseInt(s.trim))
    } catch {
      // catch Exception to catch null 's'
      case e: Exception => None
    }
  }

  val s = Seq("1", "2", "foo", "3", "bar")
  println(s.flatMap(toInt))

  def fib2(n: Int): Int = {
    @annotation.tailrec
    def loop(n: Int, prev: Int, cur: Int): Int =
      if (n == 0) prev
      else loop(n - 1, cur, prev + cur)
    loop(n, 0, 1)
  }

  def fib(n: Int): Int = {
    if (n == 0) 0
    else if (n == 1) 1
    else fib(n - 2) + fib(n - 1)
  }

  def isSorted[A](as: Array[A], gt: (A, A) => Boolean): Boolean = {
    def go(n: Int): Boolean = {
      if (n >= as.length - 1) true
      else if (gt(as(n), as(n + 1))) false
      else go(n + 1)
    }
    go(0)
  }

  println(fib2(30))
  println(fib(30))

  //for(result<-results) {
  // println(result.symbol)
  //}
  //Helper.getLastTwoTradingDates
  //getAllDailyQuote
  //val stocks = testStockListDownload()
  //testDailyQuoteDownload(stocks)
  //testHistoricalDataDownload(stocks)

  def findFirstNonRepeatCharacter(s: String): Char = {
    var result = 0.toChar
    val characterCountMap = mutable.HashMap.empty[Char, Integer]
    for (character <- s) {
      if (characterCountMap.contains(character)) {
        characterCountMap(character) = characterCountMap(character) + 1
      } else {
        characterCountMap(character) = 1
      }
    }

    val loop = new Breaks;
    loop.breakable {
      for (character <- s) {
        if (characterCountMap(character) == 1) {
          result = character
          loop.break
        }
      }
    }
    result
  }

  println(findFirstNonRepeatCharacter("sfddsewwdf"))
  println(findFirstNonRepeatCharacter("gsfddsewwdf"))

  def addTwoNumbers(l1: ListNode, l2: ListNode) = {
    var p1 = l1
    var p2 = l2
    var carry = 0
    var p3: ListNode = null

    while (p1 != null || p2 != null) {
      var val1 = 0
      var val2 = 0
      if (p1 != null) {
        val1 = p1.value
        p1 = p1.next
      }

      if (p2 != null) {
        val2 = p2.value
        p2 = p2.next
      }

      val sum = val1 + val2 + carry
      val value = sum % 10
      carry = sum / 10
      p3 = ListNode(value, p3)
    }

    if (carry > 0)
      p3 = new ListNode(carry, p3)

    p3
  }

  val l1 = ListNode(3, ListNode(4, ListNode(8, ListNode(6, null))))
  val l2 = ListNode(7, ListNode(3, ListNode(7, ListNode(9, ListNode(5, ListNode(2, null))))))

  var l3 = addTwoNumbers(l1, l2)
  while (l3 != null) {
    print(l3.value + ",")
    l3 = l3.next
  }

  println()
  
  val l4 = ListNode(3, ListNode(4, ListNode(8, ListNode(6, null))))
  val l5 = ListNode(7, ListNode(3, ListNode(7, ListNode(9, null))))

  l3 = addTwoNumbers(l4, l5)
  while (l3 != null) {
    print(l3.value + ",")
    l3 = l3.next
  }
}

case class ListNode(value: Integer,
                    next: ListNode) {
}
