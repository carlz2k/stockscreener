import akka.actor.ActorSystem
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import org.slf4j.LoggerFactory
import spray.can.Http

import spray.http._
import spray.client.pipelining._

import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}


object HttpClient {
  val log = LoggerFactory.getLogger(getClass)
  implicit val system = ActorSystem()

  import system.dispatcher

  // execution context for futures

  implicit val timeout = Timeout(300 seconds)

  def doGet(host: String, pathAndQuery: String): String = {
    val pipeline: Future[SendReceive] =
      for (
        Http.HostConnectorInfo(connector, _) <-
        IO(Http) ? Http.HostConnectorSetup(host, port = 80)
      ) yield sendReceive(connector)
    val request = Get(pathAndQuery)
    val response: Future[HttpResponse] = pipeline.flatMap(_(request))
    try {
      val httpResponse = Await.result(response, timeout.duration)
      if (httpResponse.status.isSuccess) {
        httpResponse.entity.data.asString
      } else {
        log.error("error download csv file, invalid url "
          + host + " " + pathAndQuery)
        ""
      }

    } catch {
      case e: Exception =>
        log.error("error download csv file: ", e)
        ""
    }

    //    response.onComplete {
    //      case Success(result: HttpResponse) =>
    //        result.entity.data.asString
    //      case Failure(result) =>
    //        log.error("error download csv file: " + result)
    //        ""
    //      case _ =>
    //        ""
    //    }
  }
}
