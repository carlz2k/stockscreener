import akka.actor.{ActorLogging, Actor}

class GreetingActor extends Actor with ActorLogging {
  def receive = {
    case Greeting(who) ⇒ log.info("Hello " + who)
  }
}

case class Greeting(who: String)
