package rest

import akka.io.IO
import spray.can.Http
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._

/**
 * Created by carl on 4/4/2015.
 */
object ServerLauncher extends App{
  val services = ActorSystemBean()
  implicit val system = services.system
  val service = services.apiRouterActor

  implicit val timeout = Timeout(5.seconds)
  IO(Http) ? Http.Bind(service, interface = "localhost", port = 8080)
}
