package rest

import org.json4s.{DefaultFormats, Formats}
import spray.httpx.Json4sJacksonSupport


case class Person(id: Option[Long], firstName: String, lastName: String, age: Int)

/**
 * Implements spray-json support so Person case class can be marshalled
 * to/from json when accepting and completing requests.  By having this
 * marshaller in scope an HttpService can automatically handle things
 * like List[Person] or Option[Person]
 */
object Json4sProtocol extends Json4sJacksonSupport  {
  override implicit def json4sJacksonFormats: Formats = DefaultFormats
}

