object EnumTest extends Enumeration {
  type EnumTest = Value
  val Red, Yellow, Green = Value

  def doWhat(color: EnumTest) = {
    if (color == Red) "stop"
    else if (color == Yellow) "hurry up"
    else "go"
  }
}
