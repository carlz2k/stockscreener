package helpers

import org.joda.time.DateTime

/**
 * Created by carl on 6/14/2015.
 */
object Helper {
  implicit class Map2Class(values: Map[String,String]){
    def convert[A](implicit mapper: MapConvert[A]) = mapper conv values
  }

  trait MapConvert[A]{
    def conv(values: Map[String,String]): A
  }

  def getLastTradingDate = {
    val lastTwoTradingDates = getLastTwoTradingDates
    lastTwoTradingDates(1)
  }

  def getLastTwoTradingDates = {
    var today = DateTime.now().withTimeAtStartOfDay()
    val tradeStartTime = DateTime.now().withTime(6,30,0,0)
    if(DateTime.now().compareTo(tradeStartTime)<0) {
      today = today.minusDays(1)
    }

    var start = today
    var end = today

    today.getDayOfWeek match {
      case 7 =>
        end = today.minusDays(2)
        start = end.minusDays(1)
      case 6 =>
        end = today.minusDays(1)
        start = end.minusDays(1)
      case 1 =>
        end = today
        start = end.minusDays(3)
      case _ =>
        end = today
        start = end.minusDays(1)
    }
    var result = List.empty[DateTime]
    result=result :+ start
    result=result :+ end

    result
  }
}
