import java.io.StringReader

import com.github.tototoshi.csv.CSVReader

object CsvParser {
  def parse(s:String) : List[Map[String,String]] ={
    val reader = CSVReader.open(new StringReader(s))
    reader.allWithHeaders()
  }
}