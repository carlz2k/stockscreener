import akka.actor.Actor
import akka.actor.Actor.Receive
import dao.{DailyQuoteDao, DailyQuote}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import helpers.Helper._

case class DownloadDailyQuote(symbol : String)

class DailyQuoteDownloader extends Actor{
  //http://real-chart.finance.yahoo.com
  implicit val formats = DefaultFormats // Brings in default date formats etc.

  def process(symbol: String) : DailyQuote = {
    val path = s"/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22$symbol%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback="

    val content = HttpClient.doGet("query.yahooapis.com",
      path
    )
    val json = parse(content)
    val jField = json findField {
      case JField("quote", obj) =>
        true
      case _ => false
    }
    val fieldMap = jField.get._2.values.asInstanceOf[Map[String, String]]
    val dailyQuote = fieldMap.convert[DailyQuote]

    DailyQuoteDao save dailyQuote

    println("finish download daily quote: "+symbol)
    dailyQuote
  }

  override def receive: Receive = {
    case DownloadDailyQuote(symbol)
      => process(symbol)
  }
}
