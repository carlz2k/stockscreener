import dao.Stock
import helpers.Helper._

object StockListDownloader {
  def process = {
    val content = HttpClient.doGet("www.nasdaq.com",
      "/screening/companies-by-name.aspx?letter=0&exchange=nasdaq&render=download"
    )
    val stockList = CsvParser.parse(content)
    var result = List.empty[Stock]
    for(s<-stockList) {
      val stock = s.convert[Stock]
      result ::= stock
    }
    result
  }
}
