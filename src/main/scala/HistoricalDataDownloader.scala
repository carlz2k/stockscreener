import akka.actor.Actor
import akka.actor.Actor.Receive
import dao.{QuoteDao, Quote}
import helpers.Helper
import helpers.Helper._
import org.joda.time.DateTime

case class DownloadHistoricalData(val symbol: String)

class HistoricalDataDownloader extends Actor {


  //http://real-chart.finance.yahoo.com
  def process(symbol: String) = {

    val lastTwoTradingDates = Helper.getLastTwoTradingDates

    val start = lastTwoTradingDates(0)
    val end = lastTwoTradingDates(1)

    val endMonth = end.getMonthOfYear - 1
    val endDay = end.getDayOfMonth
    val endYear = end.getYear

    val startMonth = start.getMonthOfYear - 1
    val startDay = start.getDayOfMonth
    val startYear = start.getYear

    val path = s"/table.csv?s=$symbol&d=$endMonth&e=$endDay&f=$endYear&a=$startMonth&b=$startDay&c=$startYear&g=d&ignore=.csv"

    val content = HttpClient.doGet("real-chart.finance.yahoo.com",
      path
    )
    val quoteList = CsvParser.parse(content)

    for (q <- quoteList) {
      val quote: Quote = q.convert[Quote]
      quote.quoteKey.symbol = symbol
      QuoteDao save quote
    }

    println("finished download historical quote: "+symbol)
  }

  override def receive: Receive = {
    case DownloadHistoricalData(symbol) => process(symbol)
  }

}
