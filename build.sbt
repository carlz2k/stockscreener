name := "dao.Stock Screenr"

version := "1.0"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.3.9",
  "org.apache.commons" % "commons-io" % "1.3.2",
  "io.spray" % "spray-io_2.11" % "1.3.3",
  "org.json4s" % "json4s-jackson_2.11" % "3.2.11",
  "io.spray" % "spray-can_2.11" % "1.3.3",
  "io.spray" % "spray-routing_2.11" % "1.3.3",
  "com.novus" % "salat_2.11" % "1.9.9",
  "org.slf4j" % "slf4j-api" % "1.7.12",
  "io.spray" % "spray-client_2.11" % "1.3.3",
  "com.github.tototoshi" %% "scala-csv" % "1.2.1",
  "joda-time" % "joda-time" % "2.8",
  "com.typesafe.akka" % "akka-contrib_2.11" % "2.3.11"
)